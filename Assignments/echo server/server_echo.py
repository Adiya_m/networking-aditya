import socket
from threading import Thread

def echo_server(c_socket, address):
        size = 1024
        while True:
            data = c_socket.recv(size)
            if (data == b'exit' or data == b'exit\n'):
                c_socket.close()
                break

            else :
                print(address,data)
                c_socket.send(data)


host = "127.0.1.1"
port = 7204
s= socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((host, port))
s.listen(5)
while True:
    c,a = s.accept()
    print ("connection established for " , a)
    newThread = Thread(target= echo_server,args=(c,a))
    newThread.start()
    c.send("client connected \n".encode())



