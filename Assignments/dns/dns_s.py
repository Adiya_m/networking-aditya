import socket

ip_address = '127.0.0.1'
port_no = 53


def getflags(data):


    byte1 = data[0:1]
    byte2 = data[1:2]

    rflags = ''

    Query_or_response = '1'
    OPCODE = ''
    for bit in range(1, 5):
        OPCODE += str(ord(byte1) & (1 << bit))
    Authoritatuve_Answer = '1'
    Truncation = '0'
    Recursion_Desired = '0'
    Recursion_Available = '0'
    Z = '000'    #this is reserved for future use
    Response_Code = '0000'

    return int(Query_or_response + OPCODE + Authoritatuve_Answer + Truncation + Recursion_Desired, 2).to_bytes(1, byteorder='big') + int(Recursion_Available + Z + Response_Code, 2).to_bytes(1,
                                                                                                             byteorder='big')
def getquestiondomain(data):

    domain_data = data[12:]

    state = 0
    length = 0
    domainstring = ''
    domainparts = []
    x = 0
    y = 0
    for byte in domain_data:
        if state == 1:
            if byte != 0:
                domainstring += chr(byte)
            x += 1
            if x == length:
                domainparts.append(domainstring)
                domainstring = ''
                state = 0
                x = 0
            if byte == 0:
                domainparts.append(domainstring)
                break
        else:
            state = 1
            length = byte
        y += 1

    rectype = data[y:y+2]

    return (domainparts, rectype)

def create_question(domain_name):

    question_bytes = b''

    for part in domain_name:
        length = len(part)
        question_bytes += bytes([length])

        for char in part:
            question_bytes += ord(char).to_bytes(1, byteorder='big')

    question_bytes += (1).to_bytes(2, byteorder='big')

    question_bytes += (1).to_bytes(2, byteorder='big')

    return question_bytes

def build_body(domain_name):

    body_bytes = b'\xc0\x0c'
    body_bytes = body_bytes + bytes([0]) + bytes([1])

    body_bytes = body_bytes + bytes([0]) + bytes([1])

    body_bytes += int(400).to_bytes(4, byteorder='big')
    body_bytes = body_bytes + bytes([0]) + bytes([4])

    body_bytes += bytes([7]) + bytes([7]) + bytes([7]) + bytes([7])

    return body_bytes
def create_response(data):

    TransactionID = data[:2]
    flags = getflags(data[2:4])
    Qcount = b'\x00\x01'
    QT = ''
    name,rectype = getquestiondomain(data)
    print(name);
    
    if rectype == b'\x00\x01':
        QT = 'A'

    if name == ['www','james','bond',''] :
        Acount = b'\x00\x01'
    else:
        Acount = b'\x00\x00'

    NScount = (0).to_bytes(2, byteorder='big')
    ARcount = (0).to_bytes(2, byteorder='big')

    header = TransactionID+flags+Qcount+Acount+NScount+ARcount
    print (header)
    question = create_question(name)
    print(question)
    body = build_body(name)
    print (body)

    return header+question+body;


sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
sock.bind((ip_address,port_no))

while True:
    data, address = sock.recvfrom(512)
    response = create_response(data)
    print(response)
    sock.sendto(response,address)

